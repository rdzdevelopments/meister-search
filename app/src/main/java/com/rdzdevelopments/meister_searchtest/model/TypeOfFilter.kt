package com.rdzdevelopments.meister_searchtest.model

data class TypeOfFilter (
    val filterID : Int?,
    val filterName : String
)
