package com.rdzdevelopments.meister_searchtest.model

data class Project(
    val color: String,
    val created_at: String,
    val id: Int,
    val mail_token: String,
    val name: String,
    val notes: String,
    val share_mode: Int,
    val share_token: String,
    val share_token_enabled: Boolean,
    val status: Int,
    val tasks_active_count: Int,
    val tasks_archive_count: Int,
    val tasks_complete_count: Int,
    val tasks_trash_count: Int,
    val team_id: Int,
    val token: String,
    val type: Any,
    val updated_at: String
)