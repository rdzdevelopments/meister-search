package com.rdzdevelopments.meister_searchtest.model

data class Section(
    val color: String,
    val created_at: String,
    val description: Any,
    val id: Int,
    val indicator: Int,
    val limit: Any,
    val name: String,
    val project_id: Int,
    val sequence: Double,
    val status: Int,
    val updated_at: String
)