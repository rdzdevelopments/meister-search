package com.rdzdevelopments.meister_searchtest.model

data class GetTasksResponse(
    val paging: Paging,
    val results: Results
)