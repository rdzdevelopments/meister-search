package com.rdzdevelopments.meister_searchtest.model

data class Paging(
    val current_page: Int,
    val results_per_page: Int,
    val total_pages: Int,
    val total_results: Int
)