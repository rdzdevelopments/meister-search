package com.rdzdevelopments.meister_searchtest.model


import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "task")
data class TaskUtilData(

    @PrimaryKey
    val id : Int,
    val name: String,
    val projectName: String,
    val status: Int,
    val notes: String?
    )