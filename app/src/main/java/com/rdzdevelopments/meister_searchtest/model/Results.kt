package com.rdzdevelopments.meister_searchtest.model

data class Results(
    val projects: List<Project>?,
    val sections: List<Section>?,
    val tasks: List<Task>?
)