package com.rdzdevelopments.meister_searchtest.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.rdzdevelopments.meister_searchtest.R
import com.rdzdevelopments.meister_searchtest.data.DataRepository
import com.rdzdevelopments.meister_searchtest.data.database.SingletonTaskDatabase
import com.rdzdevelopments.meister_searchtest.model.TaskUtilData
import com.rdzdevelopments.meister_searchtest.model.TypeOfFilter

class MainViewModel(val app : Application) : AndroidViewModel(app) {

    private val dataRepository = DataRepository(SingletonTaskDatabase.getInstance(app.applicationContext).taskDao)

    val query = MutableLiveData("")
    var selectedFilter = MutableLiveData(TypeOfFilter(null, app.getString(R.string.all)))

    fun simulateFetchTypeOfFiltersFromApi() : List<TypeOfFilter>{
        return listOf(
            TypeOfFilter(null, app.getString(R.string.all)),
            TypeOfFilter(1,app.getString(R.string.active)),
            TypeOfFilter(18,app.getString(R.string.archived))
        )
    }

    suspend fun getTasks(isNetworkAvailable : Boolean) = dataRepository.getTasks(isNetworkAvailable, query.value ?: "", selectedFilter.value?.filterID ?: 18)

}