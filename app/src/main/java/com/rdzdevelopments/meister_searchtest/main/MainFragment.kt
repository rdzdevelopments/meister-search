package com.rdzdevelopments.meister_searchtest.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.widget.RxTextView
import com.rdzdevelopments.meister_searchtest.base.BaseFragment
import com.rdzdevelopments.meister_searchtest.databinding.MainFragmentBinding
import com.rdzdevelopments.meister_searchtest.main.adapter.FilteringAdapter
import com.rdzdevelopments.meister_searchtest.main.adapter.TaskAdapter
import com.rdzdevelopments.meister_searchtest.model.TaskUtilData
import com.rdzdevelopments.meister_searchtest.model.TypeOfFilter
import kotlinx.coroutines.launch
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class MainFragment : BaseFragment() {

    private lateinit var binding: MainFragmentBinding
    private val mainViewModel: MainViewModel by activityViewModels()

    private lateinit var tasksResult : LiveData<List<TaskUtilData>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.fragment = this
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclers()
        inputQueryObserver()
        filterSelectedObserver()
    }

    private fun initRecyclers() {
        initFilteringRecycler()
        initTaskRecycler()
    }

    private fun initTaskRecycler() = with(binding.taskContainer) {
        layoutManager = LinearLayoutManager(requireContext())
        adapter = TaskAdapter{ createDialog(it) }
    }

    private fun initFilteringRecycler() = with(binding.filterContainer) {
        val listOfFilters = mainViewModel.simulateFetchTypeOfFiltersFromApi()
        layoutManager = LinearLayoutManager(requireContext()).apply {
            orientation = LinearLayoutManager.HORIZONTAL
        }
        adapter = FilteringAdapter { filterOnClick(it) }.apply {
            insertItems(listOfFilters)
        }
    }

    private fun filterOnClick(it: TypeOfFilter) {
        mainViewModel.selectedFilter.value = it
    }

    fun clearText() {
        binding.searchInput.text.clear()
    }

    private fun inputQueryObserver() {
        lifecycleScope.launch {
            RxTextView.textChanges(binding.searchInput)
                .debounce(400, TimeUnit.MILLISECONDS)
                .skip(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mainViewModel.query.value = it.toString()
                    fetchData()
                }
        }
    }

    private fun fetchData() {
        val emptyInput = mainViewModel.query.value?.isEmpty() == true
        if (emptyInput){
            (binding.taskContainer.adapter as TaskAdapter).clearList()
            showEmptyResult()
            setHintImage(true)
        }
        else{
            getTasks()
        }
    }

    private fun setHintImage(empty: Boolean) = with(binding.hintImage) {
        if (empty) {
            setAnimation("search_something.json")
        } else {
            setAnimation("empty_box.json")
            playAnimation()
        }
        playAnimation()
    }

    private fun filterSelectedObserver() {
        lifecycleScope.launch {
            mainViewModel.selectedFilter.observe(viewLifecycleOwner, Observer {
                fetchData()
            })
        }
    }

    private fun showLoadingBar() {
        setViewsToVisible(binding.searchProgressBar)
    }

    private fun hideLoadingBar() {
        setViewsToGone(binding.searchProgressBar)
    }

    private fun getTasks() {
        showLoadingBar()
        tasksResult = liveData {
            try {
                val response = mainViewModel.getTasks(isNetworkAvailable())
                emit(response)
            } catch (e : Exception){ }
        }
        observeResult()
    }


    private fun observeResult() {
        tasksResult.observe(viewLifecycleOwner, Observer { tasks ->
            showResultsInContainer(tasks)
            removeKeyBoard()
            hideLoadingBar()
        })
    }

    private fun showResultsInContainer(tasks : List<TaskUtilData>) {
            if (tasks.isEmpty()) {
                setHintImage(false)
                showEmptyResult()
            } else {
                showContentResult(tasks)
            }
    }

    private fun showContentResult(tasks : List<TaskUtilData>) {
        with(binding.taskContainer.adapter as TaskAdapter) {
            clearList()
            insertItems(tasks)
        }
        setViewsToVisible(binding.taskContainer)
        setViewsToGone(binding.emptyResultLayout)
    }

    private fun showEmptyResult() = with(binding) {
        setViewsToVisible(emptyResultLayout)
        setViewsToGone(taskContainer)
    }

}