package com.rdzdevelopments.meister_searchtest.main.adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rdzdevelopments.meister_searchtest.R
import com.rdzdevelopments.meister_searchtest.base.BaseRecyclerViewAdapter
import com.rdzdevelopments.meister_searchtest.databinding.FilteringAdapterBinding
import com.rdzdevelopments.meister_searchtest.model.TypeOfFilter

class FilteringAdapter(val actionOnClick : (TypeOfFilter) -> Unit) : BaseRecyclerViewAdapter<TypeOfFilter, FilteringAdapter.FilteringViewHolder>() {

    var selectedPosition : Int = 0

    inner class FilteringViewHolder(private val binding : FilteringAdapterBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                notifyItemChanged(selectedPosition)
                selectedPosition = adapterPosition
                notifyItemChanged(selectedPosition)
                actionOnClick(getItem(selectedPosition))
            }
        }

        fun bind(position: Int){
            binding.filter = getItem(position)
            setProperColor(position)
        }

        private fun setProperColor(position: Int) {
            if(selectedPosition == position){
                setItemClickedDesign()
            }
            else{
                setItemUnclickedDesign()
            }
        }

        private fun setItemClickedDesign() {
            binding.filterName.setTypeface(null, Typeface.BOLD)
            binding.card.setCardBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.primaryDarkColor))
        }

        private fun setItemUnclickedDesign() {
            binding.filterName.setTypeface(null, Typeface.NORMAL)
            binding.card.setCardBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.primaryColor))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilteringViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<FilteringAdapterBinding>(layoutInflater, R.layout.filtering_adapter, parent, false)
        return FilteringViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FilteringViewHolder, position: Int) {
        holder.bind(position)
    }


}