package com.rdzdevelopments.meister_searchtest.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rdzdevelopments.meister_searchtest.R
import com.rdzdevelopments.meister_searchtest.base.BaseRecyclerViewAdapter
import com.rdzdevelopments.meister_searchtest.databinding.TaskAdapterBinding
import com.rdzdevelopments.meister_searchtest.model.Task
import com.rdzdevelopments.meister_searchtest.model.TaskUtilData

class TaskAdapter( val actionOnClick : (String) -> Unit) : BaseRecyclerViewAdapter<TaskUtilData, TaskAdapter.TaskViewHolder>() {

    inner class TaskViewHolder(private val binding : TaskAdapterBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val taskName = getItem(adapterPosition).name
                actionOnClick(taskName)
            }
        }

        fun bind(position: Int){
            binding.task = getItem(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<TaskAdapterBinding>(layoutInflater, R.layout.task_adapter, parent, false)
        return TaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
            holder.bind(position)
    }


}