package com.rdzdevelopments.meister_searchtest.data.api

import com.rdzdevelopments.meister_searchtest.BuildConfig
import com.rdzdevelopments.meister_searchtest.model.GetTasksResponse
import retrofit2.Response
import retrofit2.http.*

interface MeisterSearchAPI {

    @Headers("Accept: application/json")
    @POST("search")
    suspend fun getTasks(
        @Header("Authorization") token : String = BuildConfig.TOKEN,
        @Query("filter") query : String,
        @Query("response_format") response : String = "object"
    ) : Response<GetTasksResponse>

}