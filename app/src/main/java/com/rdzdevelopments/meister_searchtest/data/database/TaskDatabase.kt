package com.rdzdevelopments.meister_searchtest.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rdzdevelopments.meister_searchtest.model.TaskUtilData

@Database(entities = [TaskUtilData::class], version = 1)
abstract class TaskDatabase : RoomDatabase() {

    abstract val taskDao : TaskDAO

}