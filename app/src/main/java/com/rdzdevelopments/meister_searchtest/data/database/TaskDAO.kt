package com.rdzdevelopments.meister_searchtest.data.database

import androidx.room.*
import com.rdzdevelopments.meister_searchtest.model.TaskUtilData

@Dao
interface TaskDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTasks(vararg tasks : TaskUtilData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTasks(tasks : List<TaskUtilData>)

    @Delete
    suspend fun deleteTasks(vararg tasks : TaskUtilData)

    @Update
    suspend fun updateTasks(vararg tasks : TaskUtilData)

    @Query("SELECT * FROM task WHERE name LIKE '%'||:query||'%' OR notes LIKE '%'||:query||'%' AND status = :status")
    suspend fun findTasksByQueryAndStatus(query : String, status : Int?) : List<TaskUtilData>

    @Query("SELECT * FROM task WHERE name LIKE '%'||:query||'%' OR notes LIKE '%'||:query||'%'")
    suspend fun findTasksByQuery(query : String) : List<TaskUtilData>

    suspend fun findTasks(query : String, status : Int?) : List<TaskUtilData> {
        return if(status == null){
            findTasksByQuery(query = query)
        }
        else{
            findTasksByQueryAndStatus(query = query, status = status)
        }
    }





}