package com.rdzdevelopments.meister_searchtest.data.database

import android.content.Context
import androidx.room.Room

object SingletonTaskDatabase {

    fun getInstance(context : Context) =
        Room.databaseBuilder(
            context,
            TaskDatabase::class.java,
            "task_database"
        ).build()


}