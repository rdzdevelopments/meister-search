package com.rdzdevelopments.meister_searchtest.data

import android.util.Log
import com.rdzdevelopments.meister_searchtest.data.api.RetrofitInstance
import com.rdzdevelopments.meister_searchtest.data.database.TaskDAO
import com.rdzdevelopments.meister_searchtest.model.Results
import com.rdzdevelopments.meister_searchtest.model.TaskUtilData

class DataRepository(dao : TaskDAO){

    val api = RetrofitInstance
    val database = dao

    suspend fun getTasks(internetConnection : Boolean, query: String, filter : Int) : List<TaskUtilData> {
        return if(internetConnection){
            getTasksFromApi(query, filter)
        }
        else{
            getTaskFromDatabase(query, filter)
        }
    }

    private suspend fun getTasksFromApi(query: String, filter: Int): List<TaskUtilData> {
        val response = api
            .getMeisterSearchService()
            .getTasks(query = getBuiltQuery(query, filter))

        if (response.isSuccessful){
            response.errorBody()?.let { Log.d("mytag", it.string()) }
        }

        val utilTasks = taskToTaskUtilData(response.body()?.results)
        database.insertTasks(utilTasks)

        return utilTasks
    }

    private suspend fun getTaskFromDatabase(query : String, filter: Int) : List<TaskUtilData> {
        return database.findTasks(query = query, status = filter)
    }

    private fun taskToTaskUtilData(results: Results?): List<TaskUtilData> {
        val list = mutableListOf<TaskUtilData>()
        results?.tasks?.forEach { currentTask ->
            list.add(
                TaskUtilData(
                    id = currentTask.id,
                    name = currentTask.name,
                    status = currentTask.status,
                    projectName = results.projects?.find { currentProject -> currentProject.id.equals(results.sections?.find { currentSection -> currentSection.id.equals(currentTask.section_id) }?.project_id) }?.name ?: "",
                    notes = currentTask.notes
                )
            )
        }
        return list
    }

    private fun getBuiltQuery(query: String, filter: Int): String {
        return "{\"status\":[$filter],\"text\":\"$query\"}"
    }


}