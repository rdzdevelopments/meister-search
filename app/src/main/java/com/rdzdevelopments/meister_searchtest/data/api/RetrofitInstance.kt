package com.rdzdevelopments.meister_searchtest.data.api

import com.google.gson.GsonBuilder
import com.rdzdevelopments.meister_searchtest.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitInstance {

    private val interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val client = OkHttpClient.Builder().apply {
        addInterceptor(interceptor)
    }.build()

    private fun getRetrofitInstance(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.URL_BASE)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()

    fun getMeisterSearchService(): MeisterSearchAPI =
        getRetrofitInstance()
            .create(MeisterSearchAPI::class.java)

}