package com.rdzdevelopments.meister_searchtest.base

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

abstract class BaseActivity : AppCompatActivity() {

    fun <T : AppCompatActivity> goToActivityWithDelay(classType: Class<T>, bundle: Bundle? = null, milis : Long){
        CoroutineScope(Dispatchers.Main).launch {
            delay(milis)
            goToActivity(classType, bundle)
        }
    }

    fun <T : AppCompatActivity> goToActivity(classType: Class<T>, bundle: Bundle? = null){
        startActivity(Intent(this, classType::class.java), bundle)
    }

    fun replaceFragment(frame : Int, fragment : BaseFragment, addToBackStack : Boolean = false) {
        supportFragmentManager.beginTransaction()
            .replace(frame, fragment)
            .also {
                if (addToBackStack){
                    it.addToBackStack(fragment.javaClass.name)
                }
            }
            .commit()
    }

    fun finishCurrentActivity(){
        finish()
    }

    fun showToast(string : String){
        Toast.makeText(this, string, Toast.LENGTH_LONG).show()
    }

}
