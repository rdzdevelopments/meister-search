package com.rdzdevelopments.meister_searchtest.base

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.rdzdevelopments.meister_searchtest.R


abstract class BaseFragment : Fragment() {

    var fragmentId: String = javaClass.name

    fun replaceFragment(frame: Int, fragment: BaseFragment, addToBackStack: Boolean = false) {
        parentFragmentManager.beginTransaction()
            .replace(frame, fragment)
            .also {
                if (addToBackStack) {
                    it.addToBackStack(fragmentId)
                }
            }
            .addToBackStack(null)
            .commit()
    }

    fun finishCurrentActivity() {
        requireActivity().finish()
    }

    fun setViewsToVisible(vararg viewItem: View) {
        viewItem.forEach { it.visibility = View.VISIBLE }
    }

    fun setViewsToGone(vararg viewItem: View) {
        viewItem.forEach { it.visibility = View.GONE }
    }

    fun setViewsToInvisible(vararg viewItem: View) {
        viewItem.forEach { it.visibility = View.INVISIBLE }
    }

    fun showKeyboard(editText: EditText) {
        editText.requestFocus()
        val inputManager =
            requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.postDelayed({
            inputManager.showSoftInput(
                editText,
                InputMethodManager.SHOW_IMPLICIT
            )
        }, 100)
    }

    fun removeKeyBoard() {
        val inputManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    fun isNetworkAvailable(): Boolean {
        val cm = requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo?.isConnectedOrConnecting == true
    }

    fun createDialog(taskName: String) {
        AlertDialog.Builder(requireContext()).apply {
            setMessage("")
            setCancelable(true)
            setPositiveButton(requireContext().getString(R.string.ok)) { dialog, _ ->
                dialog.dismiss()
            }
        }.let {
            it.create().apply {
                setTitle(taskName)
                show()
            }
        }
    }

    fun ImageView.setImage(url: Any?) {
        Glide.with(requireContext())
            .load(url)
            .into(this)
    }

    fun showToast(string: String) {
        Toast.makeText(requireContext(), string, Toast.LENGTH_LONG).show()
    }

    fun ImageView.rotateImage(time: Long) {
        val rotate = RotateAnimation(
            0f,
            180f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        ).apply {
            duration = time
            fillAfter = true
            interpolator = LinearInterpolator()
        }
        startAnimation(rotate)
    }

}
