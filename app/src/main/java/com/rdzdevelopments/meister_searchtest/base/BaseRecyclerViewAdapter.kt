package com.rdzdevelopments.meister_searchtest.base

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide


abstract class BaseRecyclerViewAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    var items : MutableList<T> = mutableListOf()

    fun insertItem(item : T) {
        items.add(item)
        notifyDataSetChanged()
    }

    fun insertItems(newItems : List<T>){
        items.addAll(newItems)
        notifyItemRangeInserted(items.size-1, newItems.size)
    }

    fun removeItem(item : T){
        items.indexOf(item).also {
            notifyItemRemoved(it)
            items.remove(item)
        }
    }

    fun clearList(){
        notifyItemRangeRemoved(0, items.size)
        items.clear()
    }

    fun getItem(pos : Int) : T{
        return items[pos] as T
    }

    fun ImageView.setImage(url : Any?){
        Glide.with(this.context)
            .load(url)
            .into(this)
    }

    override fun getItemCount() = items.size
}
